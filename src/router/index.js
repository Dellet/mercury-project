import Vue from 'vue'
import VueRouter from 'vue-router'
import Helloscreen from '../components/HelloScreen'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'helloscreen',
    component: Helloscreen
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: () => import('../components/Dashboard'),
    children: [{
      path: 'cabinet',
      name: 'cabinet',
      component: () => import('../components/MyCabinet'),
    }, {
      path: 'financials',
      name: 'financials',
      component: () => import('../components/MyFinancials')
    }, {
      path: 'verification',
      name: 'verification',
      component: () => import('../components/Verification')
    }, {
      path: 'myindexes',
      name: 'myindexes',
      component: () => import('../components/MyIndexes')
    }, {
      path: 'allindexes',
      name: 'allindexes',
      component: () => import('../components/AllIndexes')
    }, {
      path: 'myteam',
      name: 'myteam',
      component: () => import('../components/MyTeam')
    }, {
      path: 'ourholding',
      name: 'ourholding',
      component: () => import('../components/OurHolding')
    }, {
      path: 'organizations',
      name: 'organizations',
      component: () => import('../components/OurOrganizations')
    }, {
      path: 'shop',
      name: 'shop',
      component: () => import('../components/OurShop')
    }, {
      path: 'exchange',
      name: 'exchange',
      component: () => import('../components/OurExchange')
    }, {
      path: 'news',
      name: 'news',
      component: () => import('../components/OurNews')
    }, {
      path: 'contacts',
      name: 'contacts',
      component: () => import('../components/OurContacts')
    }]
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
